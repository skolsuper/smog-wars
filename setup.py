import os
from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='pythonRTS',
    version='0.1',
    packages=['pythonRTS'],
    include_package_data=True,
    license='MIT License',  # example license
    description='A simple Django app to store simple items with a short list of features and associated images.',
    long_description=README,
    url='http://www.example.com/',
    author='James Keys',
    author_email='skolsuper@gmail.com',
    install_requires=[line.strip() for line in open('requirements.txt')],
    classifiers=[
        'Environment :: ',
        'Framework :: ',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License', # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        # Replace these appropriately if you are stuck on Python 2.
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Games :: Real Time Strategy',
    ],
)
