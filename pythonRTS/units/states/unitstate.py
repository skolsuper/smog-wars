'''
Base class for unit states (finite state machine)
'''

import random

class UnitState:
    def __init__(self, unit):
        self._unit = unit
        self._team = unit.get_team()

    def success(self):
        '''
	Remove self from parent unit's state stack
	'''
        self._unit.cancel_order()

    def handle_projectile(self,projectile):
        '''
        Apply damage from projectiles
        '''
        attr = self._unit.get_attr()
        damage = projectile.get_damage()
        while damage > 0:
            index = random.choice(range(len(attr)))
            if attr[index] < damage:
                damage -= attr[index]
                attr[index] = 0
            else:
                attr[index] -= damage
                damage = 0
            if sum(attr) == 0:
                break
        self._unit.set_attr(attr)
