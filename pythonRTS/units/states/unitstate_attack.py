'''
State for when a unit is given an attack order. If the target is out of range
paths to the nearest cell that is in range of target.
'''

from .unitstate import UnitState
from utils.dist_between import dist_between

class Attack(UnitState):
    def __init__(self,unit,target):
        UnitState.__init__(self,unit)
        
        self._target = target
        
    def update(self):
        if self._unit.get_team().is_villain_visible(self._target):
            if dist_between(self._unit.get_pos(),self._target.get_pos()) > self._unit.get_range():
                self._unit.move_path(lambda x: dist_between(x,self._target.get_pos()) < self._unit.get_range())
            else:
                self._unit.open_fire(self._target)
        else:
            self.success()
