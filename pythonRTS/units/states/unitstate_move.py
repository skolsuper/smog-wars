'''
State for handling basic moving around.
'''

from .unitstate import UnitState
import math

class Move(UnitState):
    def __init__(self,unit,map,target_pos):
        UnitState.__init__(self,unit)
        self._map = map
        
        self._target_pos = target_pos
        current_pos = self._unit.get_pos()
        vdiff = current_pos[0]-self._target_pos[0]
        hdiff = current_pos[1]-self._target_pos[1]
        distance = math.hypot(vdiff,hdiff)
        
        angle = math.atan2(vdiff,hdiff)
        speed = self._unit.get_speed()/30.0
        self._move = (math.sin(angle)*speed,math.cos(angle)*speed)
        self._moves_remaining = distance/speed
        
    def update(self):
        if self._moves_remaining > 1:
            self._unit.change_pos(self._move)
            self._moves_remaining -= 1
        else:
            self._unit.set_pos(self._target_pos)
            self.success()

        
            
