'''
Idle state
'''

from .unitstate import UnitState
from utils.dist_between import dist_between

class Idle(UnitState):
    def __init__(self,parent):
        UnitState.__init__(self,parent)
        self._idle_time = 0
        
    def update(self):
        for villain in self._team.known_villains():
            if dist_between(self._unit.get_pos(), villain.get_pos()) < self._unit.get_range():
                self._unit.open_fire(villain)
