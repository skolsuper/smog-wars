'''
State for handling pathing. Takes a function as an argument and paths to
the nearest cell for which f(cell) == True
'''

from .unitstate import UnitState
from utils.path_BFS import PathBFS

class MovePath(UnitState):
    def __init__(self,unit,map,cell_func):
        UnitState.__init__(self,unit)
        
        current_pos = self._unit.get_grid_pos()
        self._movestack = PathBFS(map,current_pos,cell_func)
        
    def update(self):
        if self._movestack.get_length() > 0:
            self._unit.move(self._movestack.pop())
        else:
            self.success()
