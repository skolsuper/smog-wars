'''
State for shooting at things
'''

from .unitstate import UnitState

class Fire(UnitState):
    def __init__(self,unit,target_villain):
        UnitState.__init__(self,unit)
        self._target = target_villain
        self._time_to_fire = 0
        
    def update(self):
        if self._unit.get_team().is_villain_visible(self._target):
            if self._time_to_fire == 0:
                self._time_to_fire = self._unit.get_time_to_fire()
                self._unit.fire(self._target)
            else:
                self._time_to_fire -= 1
        else:
            self.success()

