'''
Type object design pattern. Stores details about a unit's type in an object
that is constructed at run time
'''

import pygame

class Type:
    def __init__(self,name,sight,speed,attack,range,time_to_fire,images):
        self._name = name
        self._sight = sight
        self._speed = speed
        self._attack = attack
        self._range = range
        self._time_to_fire = time_to_fire
        self._images = images

    def __str__(self):
        return self._name

    def get_sight(self):
        return self._sight

    def get_speed(self):
        return self._speed

    def get_attack(self):
        return self._attack

    def get_range(self):
        return self._range

    def get_time_to_fire(self):
        return self._time_to_fire

    def get_image(self,modifier):
        assert self._images.has_key(modifier), "No image"
        return self._images[modifier]

SCOUT_IMAGES = {
    "STANDARD": pygame.image.load("images/soldier.png"),
    "SELECTED": pygame.image.load("images/soldier_s.png"),
    "VILLAIN": pygame.image.load("images/soldier_v.png")
    }

TANK_IMAGES = {}

TYPES = {
    'scout': Type("Scout",8,3,1,8,20,SCOUT_IMAGES),
    'tank': Type("Tank",3,5,10,5,40,TANK_IMAGES)
    }
