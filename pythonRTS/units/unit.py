'''
Single in-game unit
'''

from utils.stack import Stack
from .states.unitstate_idle import Idle
from .states.unitstate_move import Move
from .states.unitstate_movepath import MovePath
from .states.unitstate_attack import Attack
from .states.unitstate_firing import Fire
from game.projectile import Projectile
from utils.dist_between import dist_between

BULLET_SPEED = 50

class Unit:
    def __init__(self, map, team, grid_pos, type):
        self._map = map
        self._team = team
        self._pos = grid_pos
        self._type = type
        self._attr = [
            type.get_sight(),
            type.get_speed(),
            type.get_range()
            ]
        self._attack = type.get_attack()
        self._time_to_fire = type.get_time_to_fire()
        self._selected = False
        self._visible = set([])
        self._statestack = Stack()
        
        self._statestack.push(Idle(self))
        self.update_view()

    def update(self):
        '''
        Performs one update cycle. Action depends on the unit's current state
        '''
        last_pos = self.get_grid_pos()
        self._statestack.peek().update()
        if self.get_grid_pos() != last_pos:
            self.update_view()
        if sum(self._attr) == 0:
            self._team.remove_unit(self)

    def update_view(self):
        '''
        Creates a set of all the cells that are visible to the unit and merges into the team's
        set of visible cells
        '''
        self._team.remove_visible(self._visible)
        view_distance = self.get_sight()
        grid_row,grid_col = self.get_grid_pos()
        visible = set([])
        for rowindex in range(max(0,grid_row - view_distance), min(grid_row + view_distance +1,self._map.get_height())):
            for colindex in range(max(0,grid_col - view_distance), min(grid_col + view_distance +1, self._map.get_width())):
                if dist_between((rowindex,colindex),(grid_row,grid_col)) < view_distance:
                    visible.update([(rowindex,colindex)])
        self._team.add_visible(visible)
        self._visible = visible

    def move_path(self,cell_func):
        '''
        Pushes a move_path state onto the stack. cell_func is a function that returns true or false that takes
        a cell as an argument. Paths to the nearest cell for which cell_func(cell) == True
        '''
        self._statestack.push(MovePath(self,self._map,cell_func))

    def move(self,grid_coords):
        '''
        Moves the unit in a straight line to specified coords. If the coordinates are already occupied,
        the unit will move to an adjacent cell
        '''
        while self._map.is_occupied(grid_coords):
            grid_coords = self._map.random_neighbour(grid_coords)
        self._team.update_position(self.get_grid_pos(),grid_coords,self)
        self._statestack.push(Move(self,self._map,grid_coords))

    def attack(self,target):
        self._statestack.push(Attack(self,target))

    def open_fire(self,target):
        self._statestack.push(Fire(self,target))

    def fire(self,target):
        self._map.add_projectile(Projectile(self._map, BULLET_SPEED, self.get_attack(), self._pos, target))

    def handle_projectile(self,projectile):
        self._statestack.peek().handle_projectile(projectile)
    
    def cancel_order(self):
        '''
        Removes the last order given
        '''
        if self._statestack.get_length() > 1:
            self._statestack.pop()

    def cancel_all(self):
        '''
        Return unit to idle state
        '''
        while not isinstance(self._statestack.peek(), Idle):
            self._statestack.pop()

    def get_pos(self):
        '''
        Returns the unit's exact position
        '''
        return self._pos

    def get_grid_pos(self):
        '''
        Returns the cell the unit is currently occupying
        '''
        row = int(self._pos[0])
        col = int(self._pos[1])
        return (row,col)

    def set_pos(self,pos):
        '''
	Positions the unit at given coords instantly
	'''
        self._pos = pos

    def change_pos(self,move):
        '''
	Adjusts position attributes relatively
	'''
        row, col = self._pos
        row -= move[0] #'up' values need to be negative (row 0 is top)
        col -= move[1] # as above
        self._pos = (row,col)

    def get_image(self,modifier):
        '''
        Returns pygame surface of unit image depending on state
        '''
        return self._type.get_image(modifier)

    def select(self):
        self._selected = True

    def deselect(self):
        self._selected = False

    def selected(self):
        return self._selected

    def toggle_selected(self):
        self._selected = not self._selected

    def get_team(self):
        return self._team

    def get_type(self):
        return str(self._type)

    def get_attr(self):
        return self._attr

    def set_attr(self, attr):
        self._attr = attr

    def get_sight(self):
        return self._attr[0]

    def get_speed(self):
        return self._attr[1]

    def get_attack(self):
        return self._attack

    def get_range(self):
        return self._attr[2]

    def get_time_to_fire(self):
        return self._time_to_fire

    def set_sight(self,sight):
        assert type(sight) == type(0), "Sight must be an integer"
        self._attr[0] = sight

    def set_speed(self,speed):
        assert type(speed) == type(0), "Speed must be an integer"
        self._attr[1] = speed

    def set_attack(self,attack):
        assert type(attack) == type(0), "Attack must be an integer"
        self._attack = attack

    def set_range(self,range):
        assert type(range) == type(0), "Range must be an integer"
        self._attr[2] = range

    def set_time_to_fire(self,time_to_fire):
        assert type(time_to_fire) == type(0), "Time to fire must be an integer"
        self._time_to_fire = time_to_fire
