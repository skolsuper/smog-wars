'''
Used to control a player-controlled team
'''

import pygame
from display.viewport import Viewport
from commander import Commander
from inputstate import InputState, NoSelected

class Player(Commander):
    def __init__(self,team,map,viewport,log):
        Commander.__init__(self,team,map)
        assert isinstance(viewport,Viewport), "viewport must be Viewport object"
        self._viewport = viewport
        self._inputstate = NoSelected(self,map)
        self._log = log

    def handle_input(self):
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                ctrl = pygame.key.get_mods() == 4160
                if self._viewport.is_click_in_sidebar(event.pos):
                    self._viewport.handle_click(event,ctrl)
                else:
                    click_pos = self._viewport.to_grid_coords(event.pos)
                    if click_pos[0] > self._map.get_width() or click_pos[1] > self._map.get_height():
                        pass
                    else:
                        if event.button == 1:
                            self._inputstate.handle_lmb(click_pos,ctrl)
                        elif event.button == 3:
                            self._inputstate.handle_rmb(click_pos,ctrl)
                    
    def update_team(self):
        self.handle_input()

    def change_state(self,new_state):
        assert isinstance(new_state,InputState),"Input state must be an InputState object"
        self._inputstate = new_state

    def get_team(self):
        return self._team
