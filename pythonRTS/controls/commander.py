'''
The controlling instance for a team of units in game.
'''

class Commander():
    def __init__(self,team,map):
        self._team = team
        self._map = map
