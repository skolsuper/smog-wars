'''
Classes for storing how the game will react to input in various game states
'''

class InputState:
    def __init__(self,player,map):
        self._player = player
        self._map = map
        self._team = self._player.get_team()

    def handle_lmb(self,pos,ctrl):
        pass

    def handle_rmb(self,pos,ctrl):
        pass

class UnitSelected(InputState):
    def __init__(self,player,map,unit):
        InputState.__init__(self,player,map)
        self._selected_units = [unit]

    def handle_lmb(self,pos,ctrl=False):
        clicked_unit = self._map.is_occupied(pos)

        if clicked_unit == False: # if the player clicked on empty space
            for unit in self._selected_units:
                unit.cancel_all()
                unit.move_path(lambda x: x == pos)

        elif clicked_unit in self._selected_units: #if player clicks on selected unit
            if ctrl or len(self._selected_units) == 1: #if it is the only unit or if they are holding ctrl, deselect
                self._selected_units.remove(clicked_unit)
                clicked_unit.deselect()
            else:                                   #if there are multiple units selected, reduce to just this unit
                self._selected_units = [clicked_unit]
                self._team.deselect_all()
                clicked_unit.select()

        elif clicked_unit.get_team() is self._team: # if the player clicked on an unselected unit in team
            if ctrl:
                self._selected_units.append(clicked_unit)
            else:
                self._selected_units = [clicked_unit]
                self._team.deselect_all()
            clicked_unit.select()

        else: # must have clicked an enemy
            if self._team.is_villain_visible(clicked_unit):
                for unit in self._selected_units:
                    unit.attack(clicked_unit)

        if len(self._selected_units) == 0:
            self._player.change_state(NoSelected(self._player,self._map))

    def handle_rmb(self,pos,ctrl=False):
        self._team.deselect_all()
        self._player.change_state(NoSelected(self._player,self._map))

class NoSelected(InputState):
    def __init__(self,player,map):
        InputState.__init__(self,player,map)

    def handle_lmb(self,pos,ctrl=False):
        clicked_unit = self._map.is_occupied(pos)
        if clicked_unit != False and clicked_unit.get_team() is self._team:
            clicked_unit.select()
            self._player.change_state(UnitSelected(self._player,self._map,clicked_unit))

