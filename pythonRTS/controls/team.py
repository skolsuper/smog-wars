'''
Stores a list of units for a player or cpu team.
'''

from units.type import Type
from units.unit import Unit
MAX_UNITS = 10

class Team():
    def __init__(self,map):
        self._map = map
        self._unitlist = {}
        self._visible = set([])
        self._seen = set([])
        self._timer = 0
        self._known_villains = []

    def update(self):
        self._timer += 1
        for unit in self.units():
            unit.update()
        if self._timer == 30:
            self._timer = 0
            self.update_villains()

    def add_unit(self,unit):
        assert len(self._unitlist) < MAX_UNITS, "Too many units"
        assert isinstance(unit, Unit), "Unit must be of type 'Unit'"
        assert not self.is_occupied(grid_pos), "Grid position already occupied"
        self._unitlist[unit.get_grid_pos()] = unit

    def create_unit(self,type,grid_pos):
        assert len(self._unitlist) < MAX_UNITS, "Too many units"
        assert isinstance(type, Type), "Type must be a 'Type' object"
        assert not self.is_occupied(grid_pos), "Grid position already occupied"
        self._unitlist[grid_pos] = Unit(self._map,self,grid_pos,type)

    def remove_unit(self,unit):
        for key in self._unitlist.keys():
            if self._unitlist[key] == unit:
                self._unitlist.pop(key)

    def is_villain_visible(self,villain):
        for known_villain in self._known_villains:
            if villain == known_villain:
                return True
        return False

    def known_villains(self):
        for villain in self._known_villains:
            yield villain

    def update_villains(self):
        villains = []
        for cell in self.get_visible():
            villain = self._map.is_occupied(cell)
            if villain != False and villain.get_team() is not self:
                villains.append(villain)
        self._known_villains = villains          

    def update_position(self,old_pos,new_pos,unit):
        try:
            self._unitlist[new_pos] = self._unitlist.pop(old_pos)
        except:
            for key in self._unitlist.keys():
                if self._unitlist[key] == unit:
                    self._unitlist.pop(key)
                    self._unitlist[new_pos] = unit

    def is_occupied(self,grid_coords):
        if self._unitlist.has_key(grid_coords):
            return self._unitlist[grid_coords]
        else:
            return False

    def num_units(self):
        return len(self._unitlist)

    def units(self):
        for unit in self._unitlist.values():
            yield unit

    def get_selected(self):
        for unit in self._unitlist.values():
            if unit.selected():
                yield unit

    def deselect_all(self):
        for unit in self._unitlist.values():
            unit.deselect()

    def none_selected(self):
        for unit in self._unitlist.values():
            if unit.selected():
                return False
        return True

    def get_visible(self):
        for cell in self._visible:
            yield cell

    def add_visible(self,new_set):
        self._visible.update(new_set)
        self._seen.update(new_set)

    def remove_visible(self,remove_set):
        self._visible.difference_update(remove_set)
