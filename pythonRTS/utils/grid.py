'''
Grid helper class. Stores two dimensional grid of integers
'''

class Grid:
    def __init__(self,height=10,width=10,grid=[]):
        if grid == []:
            self._grid = []
            for row in range(height):
                self._grid.append([])
                for col in range(width):
                    self._grid[row].append(0)
        else:
            self._grid = grid
        self._width = width
        self._height = height

    def __str__(self):
        str_grid = ""
        for row in range(self._height):
            str_grid += str(self._grid[row]) + "\n"
        return str_grid

    def get_width(self):
        return self._width

    def get_height(self):
        return self._height

    def is_empty(self,cell):
        row,col = cell
        assert 0 <= row < self._height, "Row index out of range"
        assert 0 <= col < self._width, "Col index out of range"
        return self._grid[row][col] == 0

    def set_full(self,cell):
        row,col = cell
        assert 0 <= row < self._height, "Row index out of range"
        assert 0 <= col < self._width, "Col index out of range"
        self._grid[row][col] = 1

    def get_square(self,cell):
        row,col = cell
        assert 0 <= row < self._height, "Row index out of range"
        assert 0 <= col < self._width, "Col index out of range"
        return self._grid[row][col]

    def set_square(self,cell,value):
        row,col = cell
        assert 0 <= row < self._height, "Row index out of range"
        assert 0 <= col < self._width, "Col index out of range"
        assert type(value) == type(0), "Value must be an integer"
        self._grid[row][col] = value

    def reinitialize(self,value):
        assert type(value) == type(0), "Value must be an integer"
        self._grid = []
        for row in range(self._height):
            self._grid.append([])
            for col in range(self._width):
                self._grid[row].append(value)

    def clone(self):
        new_grid = []
        for row in range(self._height):
            new_grid.append([])
            for col in range(self._width):
                new_grid[row].append(self._grid[row][col])
        return Grid(new_grid)
