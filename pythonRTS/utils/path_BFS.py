'''
Breadth First Seach that takes two points and
a map (grid) and returns a path between them as a stack of tuples
'''

import queue
import grid
import stack

def PathBFS(map,origin,cell_func):
    grid_height, grid_width = map.get_height(), map.get_width()
    distance_field = grid.Grid(grid_height,grid_width)
    distance_field.reinitialize(grid_height * grid_width)

    visited = grid.Grid(grid_height,grid_width)
    boundary = queue.Queue()
    boundary.enqueue(origin)
    visited.set_full(origin)
    distance_field.set_square(origin,0)
    
    path_found = False
    
    while not path_found and boundary.get_length() > 0:
        cell = boundary.dequeue()
        distance = distance_field.get_square(cell)
        for neighbour in map.eight_neighbours(cell):
            if visited.is_empty(neighbour):
                distance_field.set_square(neighbour, min(distance_field.get_square(neighbour),distance + 1))
                if cell_func(neighbour):
                    path_found = True
                    target = neighbour
                    break
                visited.set_full(neighbour)
                boundary.enqueue(neighbour)
                

    path = stack.Stack()
    if path_found:
        path.push(target)
        while True:
            cell = path.peek()
            if cell == origin:
                path.pop()
                break
            current_distance = distance_field.get_square(cell)
            check_diagonal = True
            for neighbour in map.four_neighbours(cell): #Check adjacent neighbours first as they're slightly closer
                if distance_field.get_square(neighbour) < current_distance:
                    check_diagonal = False
                    path.push(neighbour)
                    break
            if check_diagonal:
                for neighbour in map.eight_neighbours(cell):
                    if distance_field.get_square(neighbour) < current_distance:
                        path.push(neighbour)
                        break
    else:
        path.push(origin)
    return path
