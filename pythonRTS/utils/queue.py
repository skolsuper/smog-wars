'''
Queue helper class
'''

class Queue:
    def __init__(self):
        self._data = []
        self._length = 0

    def enqueue(self,item):
        self._data.insert(0,item)
        self._length += 1

    def dequeue(self):
        assert self._length >0, "Queue is empty"
        self._length -= 1
        return self._data.pop()

    def peek(self):
        assert self._length >0, "Queue is empty"
        return self._data[self._length -1]

    def get_length(self):
        return self._length
