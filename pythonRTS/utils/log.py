class Log:
    def __init__(self):
        self._log = []
        self._log.append("Initialised log")

    def __call__(self,string):
        self._log.append(string)

    def replay(self):
        for entry in self._log:
            print entry
