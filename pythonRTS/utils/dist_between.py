'''
Helper function to work out the distance between two points
'''

import math

def dist_between(p1, p2):
    y1, x1 = p1
    y2, x2 = p2
    hdiff = x1 - x2
    vdiff = y1 - y2
    return math.hypot(hdiff, vdiff)
