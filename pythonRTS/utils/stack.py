'''
Stack helper class
'''

class Stack():
    def __init__(self):
        self._data = []
        self._length = 0

    def push(self,item):
        self._data.append(item)
        self._length += 1

    def pop(self):
        assert self._length > 0, "Stack is empty"
        self._length -= 1
        return self._data.pop()

    def peek(self):
        assert self._length > 0, "Stack is empty"
        return self._data[self._length - 1]

    def get_length(self):
        return self._length
