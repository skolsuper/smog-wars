'''
Manages ferrying things between the game map grid
to the viewport and back again
'''

class Viewport:
    def __init__(self, width, height, sq_size, origin=(0,0)):
        self._height = height
        self._width = width
        self._sq_size = sq_size
        self._origin = origin

    def handle_click(self, click_event, ctrl):
        click_pos = click_event.pos
        if click_pos[1] < 20:
            if click_pos[0] > self._width - 20:
                self.zoom_out()
            elif click_pos[0] > self._width - 40:
                self.zoom_in()

    def is_click_in_sidebar(self,click_pos):
        return click_pos[0] > (self._width - 200)

    def to_grid_coords(self,screen_coords):
        return (int(screen_coords[0]/self._sq_size),int(screen_coords[1]/self._sq_size))

    def to_screen_coords(self,grid_coords):
        return (grid_coords[0]*self._sq_size,grid_coords[1]*self._sq_size)

    def get_width(self):
        return self._width

    def get_height(self):
        return self._height

    def get_sq_size(self):
        return self._sq_size

    def zoom_in(self):
        self._sq_size += 5

    def zoom_out(self):
        self._sq_size -= 5
