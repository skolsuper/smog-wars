import pygame

WHITE = pygame.Color(255,255,255)
GREY = pygame.Color(128,128,128)

PROJECTILE_IMAGES = {
    "BULLET": pygame.image.load("images/bullet.png")
    }

class Renderer:
    def __init__(self,surface,viewport,map,log):
        self._surface = surface
        self._height = self._surface.get_height()
        self._width = self._surface.get_width()
        self._viewport = viewport
        self._map = map
        self._log = log
    
    def update(self,team):
        dirty_rects = []
        square = (self._viewport.get_sq_size(),self._viewport.get_sq_size())
        for unit in team.units():
            if unit.selected():
                img = unit.get_image("SELECTED")
            else:
                img = unit.get_image("STANDARD")
            img_rect = pygame.Rect(self._viewport.to_screen_coords(unit.get_pos()),square)
            img = pygame.transform.scale(img, square)
            self._surface.blit(img,img_rect)
            dirty_rects.append(img_rect)

        for unit in team.known_villains():
            img = unit.get_image("VILLAIN")
            img_rect = pygame.Rect(self._viewport.to_screen_coords(unit.get_pos()),square)
            img = pygame.transform.scale(img, square)
            self._surface.blit(img,img_rect)
            dirty_rects.append(img_rect)
        
        for projectile in self._map.projectiles():
            img = PROJECTILE_IMAGES["BULLET"]
            img_rect = img.get_rect()
            img_rect.x, img_rect.y = self._viewport.to_screen_coords(projectile.get_pos())
            self._surface.blit(img,img_rect)
            dirty_rects.append(img_rect)
            
        return dirty_rects

    def fill_in_background(self,team):
        background_rects = []
        sq_colors = []
        sq_size = self._viewport.get_sq_size()
        
        for cell in team.get_visible():
            x_coord = cell[0] * sq_size
            y_coord = cell[1] * sq_size
            background_rects.append(pygame.Rect((x_coord,y_coord),(sq_size,sq_size)))
            sq_colors.append(128 + self._map.get_terrain(cell))
        
        for index in range(len(background_rects)):
            color = sq_colors[index]
            square = pygame.Surface((sq_size,sq_size))
            square.fill((color,color,color))
            self._surface.blit(square,background_rects[index])

        return background_rects
