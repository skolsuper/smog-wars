'''
Class for modelling projectiles (bullets and missiles)
'''

import math
import random

class Projectile:
    def __init__(self,map,speed,damage,pos,target):
        self._map = map
        self._speed = speed
        self._damage = damage
        self._pos = pos
        self._target = target
        self._target_grid_pos = target.get_grid_pos()

        self._target_pos = self._target.get_pos()
        vdiff = self._pos[0]-self._target_pos[0]
        hdiff = self._pos[1]-self._target_pos[1]
        distance = math.hypot(vdiff,hdiff)
        
        angle = math.atan2(vdiff,hdiff)
        speed = self._speed/30.0
        
        self._move = (math.sin(angle)*speed,math.cos(angle)*speed)
        self._moves_remaining = distance/speed

    def update(self):
        y, x = self._pos
        vdiff, hdiff = self._move
        y -= vdiff #negative because zero row is at the top
        x -= hdiff
        self._pos = y, x
        if self._moves_remaining > 1:
            self._moves_remaining -= 1
        else:
            self._pos = self._target_pos
            self.impact()

    def impact(self):
        victim = self._map.is_occupied(self._target_grid_pos)
        if victim != False:
            victim.handle_projectile(self)
        self._map.remove_projectile(self)

    def get_damage(self):
        return self._damage

    def get_pos(self):
        return self._pos
