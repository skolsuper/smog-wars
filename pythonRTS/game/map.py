'''
Composition of various grids that make up the game map
'''

import random

from utils.grid import Grid
from controls.team import Team

TEAMS = { 'RED': 0,
          'BLUE': 1,
          'GREEN': 2,
          'YELLOW': 3 }
MAX_TEAMS = 4

class GameMap():
    def __init__(self,height,width,num_teams):
        assert num_teams <= MAX_TEAMS, "Max number of teams is " + MAX_TEAMS
        self._terrain = Grid(height,width)
        self._obstacles = Grid(height,width)
        self._projectiles = []
        self._teams = []
        for dummy_index in range(num_teams):
            self._teams.append(Team(self))

    def get_height(self):
        return self._terrain.get_height()

    def get_width(self):
        return self._terrain.get_width()

    def get_team(self, team):
        return self._teams[TEAMS[team]]

    def add_projectile(self, projectile):
        self._projectiles.append(projectile)

    def remove_projectile(self, projectile):
        self._projectiles.remove(projectile)

    def projectiles(self):
        for projectile in self._projectiles:
            yield projectile

    def teams(self):
        for team in self._teams:
            yield team

    def get_terrain(self,grid_coords):
        return self._terrain.get_square(grid_coords)

    def is_obstacle(self,grid_coords):
        return not self._obstacles.is_empty(grid_coords)

    def is_occupied(self,grid_coords):
        '''
        Check if a square contains a unit
        '''
        for team in self._teams:
            if team.is_occupied(grid_coords):
                return team.is_occupied(grid_coords)
        return False

    def four_neighbours(self,grid_coords):
        row, col = grid_coords
        assert 0 <= row < self.get_height(), "Row out of bounds"
        assert 0 <= col < self.get_width(), "Col out of bounds"
        neighbours = []
        if row != 0:
            neighbours.append((row-1,col))
        if row != self.get_height():
            neighbours.append((row+1,col))
        if col != 0:
            neighbours.append((row,col-1))
        if col != self.get_width():
            neighbours.append((row,col+1))
        return neighbours

    def eight_neighbours(self,grid_coords):
        '''
        Returns a list of tuples that are neighbours of a cell
        '''
        row, col = grid_coords
        assert 0 <= row < self.get_height(), "Row out of bounds"
        assert 0 <= col < self.get_width(), "Col out of bounds"
        neighbours = []
        for row_index in range(max(0,row - 1), min(self.get_height(),row + 2)):
            for col_index in range(max(0,col - 1), min(self.get_width(),col + 2)):
                neighbours.append((row_index,col_index))
        neighbours.remove(grid_coords)
        return neighbours

    def random_neighbour(self,grid_coords):
        '''
        Choose a random neighbour of given grid coords
        '''
        return random.choice(self.eight_neighbours(grid_coords))
