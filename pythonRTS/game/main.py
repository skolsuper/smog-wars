import pygame
from display.render import Renderer
from .map import GameMap
from units.type import TYPES
from controls.player import Player
from utils.log import Log
from display.viewport import Viewport

gamelog = Log()
pygame.init()
DISPLAYSURF = pygame.display.set_mode((800,600))
clock = pygame.time.Clock()
FRAMES_PER_SECOND = 30

MAPSIZE = 20
NUM_TEAMS = 2

main_window = Viewport(800,600,30)
game_map = GameMap(MAPSIZE,MAPSIZE,NUM_TEAMS)
renderer = Renderer(DISPLAYSURF,main_window,game_map,gamelog)

playerteam = game_map.get_team('RED')
for iii in range(5):
    playerteam.create_unit(TYPES['scout'],(0,iii))

cputeam = game_map.get_team('BLUE')
cputeam.create_unit(TYPES['scout'],(15,15))

player1 = Player(playerteam,game_map,main_window,gamelog)
commander_list = [player1]

def main():
    while(True):
        for event in pygame.event.get(pygame.QUIT):
            pygame.quit()
            return 0

        for commander in commander_list:
            commander.update_team()

        for team in game_map.teams():
            team.update()

        for projectile in game_map.projectiles():
            projectile.update()

        dirty_rects = renderer.fill_in_background(player1.get_team())
        dirty_rects.extend(renderer.update(player1.get_team()))
        pygame.display.update(dirty_rects)
        deltat = clock.tick(FRAMES_PER_SECOND)

        text = "FPS: {0:.2f}".format(clock.get_fps())
        pygame.display.set_caption(text)

print main()
